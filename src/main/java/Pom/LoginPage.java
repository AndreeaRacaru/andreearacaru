package Pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

   public  WebDriver driver;
   public static String url = "https://www.saucedemo.com";

    By usernameInput = By.id("user-name");
    By paswoardInput = By.id("password");
    By loginButton = By.id("login-button");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void enterUsername (String username){

        driver.findElement(usernameInput).sendKeys(username);

    }

    public void enterPasswoard (String passwoard){

        driver.findElement(paswoardInput).sendKeys(passwoard);

    }

    public void clickLogin (){

        driver.findElement(loginButton).click();

    }

    //metoda pt mesaj de eroare la login negativ

}
