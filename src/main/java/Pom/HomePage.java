package Pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

   WebDriver driver;
   By appLogo = By.className("app_logo");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public String getLogoTesxt(){

        return driver.findElement(appLogo).getText();

    }
}
