package utils;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTestClass {
    protected static WebDriver driver;

    @Before
    public void setup() {
        System.out.println("Setup method");
        driver = new ChromeDriver();
    }

    @After
    public void cleanupBrowser() {
        System.out.println("Cleanup method");
        driver.quit();
    }
}
