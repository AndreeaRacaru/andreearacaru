import Pom.HomePage;
import Pom.LoginPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginPageTests {

    WebDriver driver;

    @Before
    public void setup() {
        driver = new ChromeDriver();

    }

    @After
    public void clenup() {
        driver.quit();

    }

    @Test
    public void SuccessLogin() throws InterruptedException {
        driver.get("https://www.saucedemo.com/");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterUsername("standard_user");
        loginPage.enterPasswoard("secret_sauce");
        loginPage.clickLogin();
        Thread.sleep(5000);
        Assert.assertEquals("https://www.saucedemo.com/inventory.html", driver.getCurrentUrl());

        HomePage homePage = new HomePage(driver);

        Assert.assertEquals("Swag Labs", homePage.getLogoTesxt());

    }

    @Test
    public void LoginWithoutUsername() throws InterruptedException {
        driver.get("https://www.saucedemo.com/");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.enterUsername("standard_user");
        loginPage.enterPasswoard("secret_sauce");
        loginPage.clickLogin();
        Thread.sleep(5000);
        Assert.assertEquals("https://www.saucedemo.com/inventory.html", driver.getCurrentUrl());

        HomePage homePage = new HomePage(driver);

        Assert.assertEquals("Swag Labs", homePage.getLogoTesxt());


    }
}