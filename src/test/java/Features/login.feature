Feature: Login test scenarios

  Scenario: Login with valid credentiales
    Given I am on Login Page
    When I enter username "standard_user"
    When I enter password "secret_sauce"
    When I click on Login button

    Then I navigate to Swag Labs page