package steps;

import Pom.HomePage;
import Pom.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;


import static steps.Hooks.driver;

public class LoginSteps {
    LoginPage loginPage;
    

    @Given("I am on Login Page")
    public void i_am_on_login_page() {
        System.out.println("Be on Login Page");

        driver.get(LoginPage.url);
        loginPage = new LoginPage(driver);

    }
    @When("I enter username {string}")
    public void i_enter_username(String username) {
        System.out.println("Enter username");
        loginPage.enterUsername(username);

    }
    @When("I enter password {string}")
    public void i_enter_password(String password) {
        System.out.println("Enter password");
        loginPage.enterPasswoard(password);
    }
    @When("I click on Login button")
    public void i_click_on_login_button() {
        System.out.println("Click on Login button");

        loginPage.clickLogin();

    }
    @Then("I navigate to Swag Labs page")
    public void i_navigate_to_swag_labs_page() {
        System.out.println("true");
        HomePage homePage = new HomePage(driver);
        Assert.assertEquals("https://www.saucedemo.com/inventory.html", driver.getCurrentUrl());



    }



}
