package steps;

import io.cucumber.java.Before;
import io.cucumber.java.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hooks {

    public static WebDriver driver;
    @Before
    public void setup() {
        System.out.println("Initialize driver with UI and Login");
        driver = new ChromeDriver();
    }

    @After
    public void clenup() {
        System.out.println("Quit the page");
        driver.close();
        driver.quit();
    }
}
